Created By :
1. Ardiana Abdul Gumelar - Universitas Alma Ata Yogyakarta 
2. Risky Kurniawan - Universitas Adhirajasa Reswara Sanjaya

Project (GITS-TUGAS-X-XI)
1. CekString.go -> Mengecek karakter string, reverse karakter, Kapital pada karakter pertama dan non kapital pada karakter selanjutnya
2. BangunDatar.go -> Program penghitungan luas dan keliling dari persegi dan segitiga (spesifik segitiga siku - siku)
