package main

import (
	"fmt"
	"strings"
)

type char struct {
	a string
}

func main() {
	var coba_lagi string
	a := char{}
	fmt.Println("========================")
	fmt.Println("        CEK HURUF       ")
	fmt.Println("========================")
	fmt.Println("123oll4eH5 =>", Check("123oll4eH5"))
	fmt.Println("Red =>", Check("Red"))
	fmt.Println("1234 =>", Check("1234"))
coba:
	fmt.Println("========================")
	fmt.Print("Masukan Karakter : ")
	fmt.Scanf("%s\n", &a.a)
	fmt.Println("========================")
	fmt.Println("Hasil : ", Check(a.a))
	fmt.Println("========================")
lanjut:
	fmt.Print("Apakah Anda ingin menambahkan karakter lagi ? (Y/T) : ")
	fmt.Scanf("%s\n", &coba_lagi)

	coba_lagi = strings.ToUpper(coba_lagi)

	if coba_lagi == "Y" {
		goto coba
	} else if coba_lagi != "Y" && coba_lagi != "T" {
		fmt.Println("keyword salah")
		goto lanjut
	}
	fmt.Println("========================")
	fmt.Println("     Terima Kasih       ")
	fmt.Println("========================")
}

func Check(character string) string {
	var filter, result string = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz", ""

	for c := 0; c < len(character); c++ {
		for f := 0; f < len(filter); f++ {
			if character[c] == filter[f] {
				result = strings.ToUpper(string(character[c])) + strings.ToLower(result)
			}
		}
	}

	if len(result) == 0 {
		return "Error(\" Harus terdapat string \")"
	}

	return result
}
