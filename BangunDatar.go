package main

import (
	"fmt"
	"math"
)

type segitiga struct {
	alas, tinggi int
}
type persegi struct {
	sisi int
}

func main() {
ToMenu:
	var menu, submenu int
	fmt.Println("=============================")
	fmt.Println("    HITUNG BANGUN DATAR      ")
	fmt.Println("=============================")
	fmt.Println("Pilihan Menu :")
	fmt.Println("1. Luas segitiga")
	fmt.Println("2. Keliling segitiga")
	fmt.Println("3. Luas Persegi")
	fmt.Println("4. Keliling Persegi")
	fmt.Println("0. Keluar")
	fmt.Println("=============================")
	fmt.Print("Masukan Menu :	")
	fmt.Scanf("%d\n", &menu)

PilMenu:
	if menu == 0 {
		fmt.Println("=============================")
		fmt.Println("        Terimakasih")
		fmt.Println("=============================")
	} else if menu == 1 {
		MenuSegitiga("luas")
	} else if menu == 2 {
		MenuSegitiga("keliling")
	} else if menu == 3 {
		MenuPersegi("luas")
	} else if menu == 4 {
		MenuPersegi("keliling")
	} else {
		fmt.Println("=============================")
		fmt.Println("Keyword anda salah")
		fmt.Println("=============================")
	}

	if menu != 0 {
	ToSubMenu:
		fmt.Println("9. Kembali Ke Menu")
		fmt.Println("0. Keluar")
		fmt.Println("=============================")
		fmt.Print("Pilihan anda : ")
		fmt.Scanf("%d\n", &submenu)
		if submenu == 9 {
			goto ToMenu
		} else if submenu == 0 {
			menu = 0
			goto PilMenu
		} else {
			fmt.Println("=============================")
			fmt.Println("Keyword anda salah")
			fmt.Println("=============================")
			goto ToSubMenu
		}
	}
}

func MenuSegitiga(operasi string) {
	s := segitiga{}
	fmt.Println("=============================")
	if operasi == "luas" {
		fmt.Println("     MENGHITUNG LUAS")
	} else if operasi == "keliling" {
		fmt.Println("   MENGHITUNG KELILING")
	}
	fmt.Println("   SEGITIGA SIKU - SIKU")
	fmt.Println("=============================")
	fmt.Print("Masukan Alas   : ")
	fmt.Scanf("%d\n", &s.alas)
	fmt.Print("Masukan Tinggi : ")
	fmt.Scanf("%d\n", &s.tinggi)
	fmt.Println("=============================")
	if operasi == "luas" {
		fmt.Println("Luasnya adalah :", s.HitungSegitiga(operasi))
	} else if operasi == "keliling" {
		miring := s.BidangMiring()
		fmt.Printf("Bidang Miring  : %.2f\n", miring)
		fmt.Printf("Keliling       : %.2f\n", s.HitungSegitiga(operasi))
	}
	fmt.Println("=============================")
}

func (s segitiga) BidangMiring() float64 {
	var miring float64 = float64((s.alas * s.alas) + (s.tinggi * s.tinggi))

	return math.Sqrt(miring)
}

func (s segitiga) HitungSegitiga(operasi string) float64 {
	if operasi == "luas" {
		return 0.5 * float64(s.alas) * float64(s.tinggi)
	} else if operasi == "keliling" {
		miring := s.BidangMiring()
		return miring + float64(s.alas) + float64(s.tinggi)
	}
	return 0
}
func MenuPersegi(hitung string) {
	p := persegi{}
	fmt.Println("=============================")
	if hitung == "luas" {
		fmt.Println("    MENGHITUNG LUAS")
	} else if hitung == "keliling" {
		fmt.Println("   MENGHITUNG KELILING")
	}
	fmt.Println("          PERSEGI ")
	fmt.Println("=============================")
	fmt.Print("Masukan sisi  : ")
	fmt.Scanf("%d\n", &p.sisi)
	fmt.Println("=============================")
	if hitung == "luas" {
		fmt.Println("Luasnya adalah :", p.hitungpersegi(hitung))
	} else if hitung == "keliling" {
		fmt.Println("Keliling       :", p.hitungpersegi(hitung))
	}
	fmt.Println("=============================")
}

func (p persegi) hitungpersegi(hitung string) int {
	if hitung == "luas" {
		return p.sisi * p.sisi
	} else if hitung == "keliling" {
		return p.sisi * 4
	}
	return 0
}
